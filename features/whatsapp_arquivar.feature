# language: pt

Funcionalidade: Arquivar

    Para esconder uma conversa da tela de Conversas
    Como um usuário do aplicativo WhatsApp
    Eu quero poder Arquivar uma conversa de um contato ou conversas de um grupo

    NARRATIVA

    O usuário pode esconder uma conversa de qualquer contato ou grupo
    utilizando a função Arquivar

    FORA DE ESCOPO
    - Desarquivamento de conversas ou grupos

    Cenário: Arquivando uma conversa de um contato
        Dado que estou na tela Conversas
        Quando eu pressionar o botão Editar
        E eu selecionar a conversa do contato "Bruno Cavallaro"
        E eu pressionar o botão Arquivar
        Então a conversa é removida de tela de Conversas

    Cenário: Arquivando um grupo
        Dado que estou na tela Conversas
        Quando eu pressionar o botão Editar
        E eu selecionar o grupo "Amigos do Bruno"
        E eu pressionar o botão Arquivar
        Então o grupo é removido de tela de Conversas
