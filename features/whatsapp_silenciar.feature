# language: pt

Funcionalidade: Silenciar

    Para evitar receber notificações de mensagens enviadas por um contato
    Como um usuário do aplicativo WhatsApp
    Eu quero poder Silenciar o contato por um tempo pré-determinado

    NARRATIVA

    O contato pode ser silenciado por 8 horas
    O contato pode ser silenciado por 1 semana
    O contato pode ser silenciado por 1 ano
    A opção Silenciar do contato pode ser desligada a qualquer momento

    FORA DE ESCOPO
    - Validar o auto-desligamento da função Silenciar após timeframe escolhido

    Cenário: Silenciando um contato por 8 horas
        Dado que estou na tela de propriedades de um contato
        Quando eu pressionar a opção Silenciar
        E eu escolher a opção "8 horas"
        Então o app informa que o contato está silenciado até determinada hora

    Cenário: Silenciando um contato por 1 semana
        Dado que estou na tela de propriedades de um contato
        Quando eu pressionar a opção Silenciar
        E eu escolher a opção "1 semana"
        Então o app informa que o contato está silenciado até determinada data e hora

    Cenário: Silenciando um contato por 1 ano
        Dado que estou na tela de propriedades de um contato
        Quando eu pressionar a opção Silenciar
        E eu escolher a opção "1 ano"
        Então o app informa que o contato está silenciado até determinada data e hora

    Cenário: Desligando opção Silenciar de um contato
        Dado que estou na tela de propriedades de um contato
        Quando eu pressionar a opção Silenciar
        E eu escolher a opção "Não silenciar"
        Então o app informa que o contato não está silenciado
